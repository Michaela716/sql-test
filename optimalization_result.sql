--- Point to parts that could be optimized
--- Feel free to comment any row that you think could be optimize/adjusted in some way!
--- The following query is from SAP HANA but applies to any DB
--- Do not worry if the tables/columns are not familiar to you 
----   -> you do not need to interpret the result (in fact the query does not reflect actual DB content)
SELECT 
	RSEG.EBELN,
	RSEG.EBELP,
    RSEG.BELNR,
    RSEG.AUGBL AS AUGBL_W,
    LPAD(EKPO.BSART,6,0) as BSART,
	BKPF.GJAHR,
	BSEG.BUKRS,
	BSEG.BUZEI,
	BSEG.BSCHL,
	BSEG.SHKZG,
    CASE WHEN BSEG.SHKZG = 'H' THEN (-1) * BSEG.DMBTR ELSE BSEG.DMBTR END AS DMBTR,
    COALESCE(BSEG.AUFNR, 'Kein SM-A Zuordnung') AS AUFNR,
    COALESCE(LFA1.LAND1, 'Andere') AS LAND1, 
    LFA1.LIFNR,
    LFA1.ZSYSNAME,
    BKPF.BLART as BLART, 	--> I don't see the point to name the column twice with the same name
    BKPF.BUDAT as BUDAT,	--> I don't see the point to name the column twice with the same name
    BKPF.CPUDT as CPUDT 	--> I don't see the point to name the column twice with the same name
FROM "DTAG_DEV_CSBI_CELONIS_DATA"."dtag.dev.csbi.celonis.data.elog::V_RSEG" AS RSEG
LEFT JOIN "DTAG_DEV_CSBI_CELONIS_WORK"."dtag.dev.csbi.celonis.app.p2p_elog::__P2P_REF_CASES" AS EKPO ON 1=1   --> I'd put this left join to the very end of script after join of LFA1 table.
    AND RSEG.ZSYSNAME = EKPO.SOURCE_SYSTEM
    AND RSEG.MANDT = EKPO.MANDT
    AND RSEG.EBELN || RSEG.EBELP = EKPO.EBELN || EKPO.EBELP  --> It looks like it unnecessarily joins 2 separate columns together in order to match them with the same columns from other table,
    														-- instead I'd simply match them as 2 separate conditions:   and RSEG.EBELN = EKPO.EBELN
                                                            															--and RSEG.EBELP = EKPO.EBELP
INNER JOIN "DTAG_DEV_CSBI_CELONIS_DATA"."dtag.dev.csbi.celonis.data.elog::V_BKPF" AS BKPF ON 1=1
    AND BKPF.AWKEY = RSEG.AWKEY
    AND RSEG.ZSYSNAME = BKPF.ZSYSNAME
    AND RSEG.MANDT in ('200')		--> I'm not sure if it helps to optimalize query better, but if you have only one value, you can write it as a simple equation:  AND RSEG.MANDT = '200'
INNER JOIN "DTAG_DEV_CSBI_CELONIS_DATA"."dtag.dev.csbi.celonis.data.elog::V_BSEG" AS BSEG ON 1=1
    AND DATS_IS_VALID(BSEG.ZFBDT) = 1
    AND BSEG.KOART = 'K'
    AND CAST(BSEG.GJAHR AS INT) = 2020  --> I'm not sure why there's cast function here, if it's string I'd replace it by: BSEG.GJAHR = '2020'
    AND BKPF.ZSYSNAME = BSEG.ZSYSNAME
    AND BKPF.MANDT = BSEG.MANDT
    AND BKPF.BUKRS = BSEG.BUKRS
    AND BKPF.GJAHR = BSEG.GJAHR
    AND BKPF.BELNR = BSEG.BELNR
    AND BSEG.DMBTR*-1 >= 0   --> I don't think it's necessary to do this operation, when you're looking for not positive numbers: BSEG.DMBTR <= 0
    
INNER JOIN (SELECT * FROM "DTAG_DEV_CSBI_CELONIS_DATA"."dtag.dev.csbi.celonis.data.elog::V_LFA1" AS TEMP
            WHERE TEMP.LIFNR > '020000000') AS LFA1 ON 1=1   --> I don't know if joined table is big or not, but I'd select only those columns I'm using (ZSYSNAME, LIFNR, MANDT, LAND1)
    AND BSEG.ZSYSNAME = LFA1.ZSYSNAME
    AND BSEG.LIFNR=LFA1.LIFNR
    AND BSEG.MANDT=LFA1.MANDT
    AND LFA1.LAND1 in ('DE','SK')  --> this condition can be put within inner select as condition in where clause, which may cause SQL to run through less rows when joining tables
;



-- ////OPTIMALIZATION\\\\ --
/*I'm use to write scripts using "with construction", because I used to work with large tables and I created very complex and long scripts (or procedures) and I found
it easier to read it like that.
In this case it doesnt look like very complex script, but it may contain large tables.
I'd move "left join" (EKPO table) to the end of the script, because it may contain information displayed in more than 1 row and it should be easier for SQL to evaluate
inner joins first and left join last. Also it may cause the result to be a mess. 
So the order of the tables would be:
from RSEG...
join BKPF...
join BSEG...
join LFA1... 
left join EKPO, 
since other tables are always joined to previous one.

In order to optimalize it more, I'd look at the matched columns - the most essential is matching primary keys and indexed columns 
and then other columns if it's still needed and doesn't prolong evaluation. */
