--TASK 1--
--Write query which will match contacts and orders to our customers

SELECT cu.customer_id, cu.last_name, cu.first_name, cu.referred_by_id,
		co.address, co.city, co.phone_number, co.email,
        o.order_id, o.item, o.order_value, o.order_currency, o.order_date
FROM customers 		cu
join contacts 		co on co.customer_id = cu.customer_id
join orders			o  on o.customer_id = cu.customer_id
;

-- if I only want customers who have at least one order, I'd use inner join
-- but if I want to see for some reason the whole table, even the customers who has no order, then I'd use left join like I used in an example below: 

SELECT cu.customer_id, cu.last_name, cu.first_name, cu.referred_by_id,
		co.address, co.city, co.phone_number, co.email,
        o.order_id, o.item, o.order_value, o.order_currency, o.order_date
FROM customers 			cu
left join contacts 		co on co.customer_id = cu.customer_id
left join orders		o  on o.customer_id = cu.customer_id
;


--TASK 2--
-- There is  suspision that some orders were wrongly inserted more times. Check if there are any duplicated orders. If so, return duplicates with the following details:
-- first name, last name, email, order id and item

SELECT first_name, last_name, email, order_id, item /*, count(*) as count*/ 
FROM
	(	SELECT cu.first_name, cu.last_name, co.email, o.order_id, o.item						
		FROM customers 		cu
		join contacts 		co on co.customer_id = cu.customer_id
		join orders			o  on o.customer_id = cu.customer_id
	)
group by first_name, last_name, email, order_id, item
having count(*) > 1
;


--TASK 3-	
-- As you found out, there are some duplicated order which are incorrect, adjust query from previous task so it does following:
-- Show first name, last name, email, order id and item
-- Does not show duplicates.
-- Order result by customer last name

SELECT distinct cu.first_name, cu.last_name, co.email, o.order_id, o.item
FROM customers 		cu
join contacts 		co on co.customer_id = cu.customer_id
join orders			o  on o.customer_id = cu.customer_id
order by cu.last_name
;


--TASK 4--
--Our company distinguishes orders to sizes by value like so:
--order with value less or equal to 25 euro is marked as SMALL
--order with value more than 25 euro but less or equal to 100 euro is marked as MEDIUM
--order with value more than 100 euro is marked as BIG
--Write query which shows only three columns: full_name (first and last name divided by space), order_id and order_size
--Make sure the duplicated values are not shown

SELECT distinct cu.first_name || ' ' || cu.last_name as full_name, o.order_id, -- o.order_value,
		case when o.order_value <= 25 then 'SMALL'
        	when o.order_value > 25 and o.order_value <= 100 then 'MEDIUM'
            when o.order_value > 100 then 'BIG'    -- can be replaced by "else 'BIG' ", but sometimes there may be dataset errors you won't find when you use else statement
            end as order_size
FROM customers 		cu
join contacts 		co on co.customer_id = cu.customer_id
join orders			o  on o.customer_id = cu.customer_id
;


--TASK 5--
-- Show all items from orders table which containt in their name 'ea' or start with 'Key'

SELECT distinct o.item
FROM orders o
where item like '%ea%' 
or item like 'Key%'
;


--TASK 6--
-- Please find out if some customer was referred by already existing customer
-- Return results in following format "Customer Last name Customer First name" "Last name First name of customer who recomended the new customer"

SELECT c.last_name ||' '|| c.first_name as customer, 
		cus.last_name ||' '|| cus.first_name as referred_by
FROM customers 			c
left join customers		cus 	on cus.customer_id = c.referred_by_id
where c.referred_by_id > 0
;